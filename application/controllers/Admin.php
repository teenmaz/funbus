<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {    
	protected $data = array();
    public function __construct(){
        parent::__construct();
        $this->data['asset_url'] = asset_url();
        if(!$this->session->userdata('logged_in')){
            redirect('auth', 'refresh');
        }
        $this->data['session_data'] = $this->session->userdata('logged_in');
        $this->load->model('Ve_m','ve',TRUE);
    } 
    
    public function index(){
        $this->smarty->view('index.tpl', $this->data);
    }


    public function bus($row_id = 0){

        $this->data['lotrinh'] = $this->ve->lotrinh();

        if($row_id!==0){
            $data_ve = $this->ve->tour_bus($row_id);
        }else{
            $data_ve = $this->ve->get_ve();
        }
        $this->data['ve_xe'] = $data_ve;

        $this->data['tour_id'] = $row_id;
        
        $this->smarty->view('bus.tpl', $this->data);
    }

    public function change_status($row_id){
        $data = $this->ve->change_status($row_id);
        echo $data;
    }
	
}
