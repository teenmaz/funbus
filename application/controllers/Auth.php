<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {    
	protected $data = array();
    public function __construct(){
        parent::__construct();
        $this->data['asset_url'] = asset_url();
        $this->load->model('User_m','user',TRUE);
    } 
    
    public function index(){
        if($this->session->userdata('logged_in')){
            redirect('admin', 'refresh');
        }
        $this->smarty->view('login.tpl', $this->data);
    }

    public function login(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
        if($this->form_validation->run() == FALSE){
            //Field validation failed.  User redirected to login page
            $this->smarty->view('login.tpl', $this->data);
        }
        else{
            //Go to private area
            redirect('admin', 'refresh');
        }
    }

   public function logout(){
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('auth', 'refresh');
   }

   function check_database($password){
       $username = $this->input->post('username');
        //query the database
        $result = $this->user->login($username, $password);
        if($result){
            $sess_array = array();
            foreach($result as $row){
                $sess_array = array(
                    'id' => $row->MANV,
                    'tennv' => $row->TENNV,
                    'macn'=>$row->MACN,
                    'machucvu'=>$row->MACHUCVU,
                    'emailnv'=>$row->EMAILNV,
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return true;
        }else{
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
   }
	
}
