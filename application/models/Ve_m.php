<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ve_m extends CI_Model {
    
    public function get_ve(){
        $this->db->from('ve');
        $query = $this -> db -> get();
        return $query->result();
    }

    public function change_status($row_id){
        $data = array(
               'TrangThai' => 1
        );
        $this->db->where('MAVE', $row_id);
        return $this->db->update('ve', $data); 
    }

    public function tour_bus($row_id){
        $this->db->select('*');
            $this->db->from('ve ve'); 
            $this->db->join('lotrinh lo', 've.MALOTRINH=lo.MALOTRINH', 'left');
            $this->db->where('lo.MALOTRINH',$row_id);
            $this->db->order_by('lo.MALOTRINH','asc');         
            $query = $this->db->get(); 
            if($query->num_rows() != 0){
                return $query->result();
            }
            
    }

    public function lotrinh(){
        $this->db->from('lotrinh');
        $query = $this -> db -> get();
        return $query->result();
    }

}