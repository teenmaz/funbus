{extends file="./layout/index.tpl"}
{block name=navbar}
    {include file="./part/navbar.tpl"}
{/block}
{block name=sidebar}
    {include file="./part/sidebar.tpl"}
{/block}
{block name=content}
    {include file="./part/summary.tpl"}
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default panel-table">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col col-xs-6">
                            <h3 class="panel-title">Vé xe</h3>
                        </div>
                        <div class="col col-xs-4">
                            <label for="sel1">Lộ trình:</label>
                                <select class="form-control" id="lotrinh">
                                    <option value="0">All</option>
                                    {foreach from=$lotrinh key=index item=row}
                                        {if $tour_id eq $row->MALOTRINH}
                                            <option selected value="{$row->MALOTRINH}">{$row->TENLOTRINH}</option>
                                            {else}
                                            <option value="{$row->MALOTRINH}">{$row->TENLOTRINH}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                            </label>    
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-list">
                        <thead>
                            <tr>
                                <th><em class="fa fa-cog"></em></th>
                                <th class="hidden-xs">Mã Vé</th>
                                <th>Tên KH</th>
                                <th>Giờ khởi hành</th>
                                <th>Ngày</th>
                                <th>Mã ghế</th>
                                <th>Trạng thái</th>
                            </tr> 
                        </thead>
                        <tbody>
                            {foreach from=$ve_xe key=index item=row}
                                <tr>
                                    <td align="center">
                                        {if $row->TrangThai eq 1}
                                            <a data-id="{$row->MAVE}" href="javascript:;" class="check_ve btn btn-success">
                                                <em class="fa fa-check"></em>
                                            </a>
                                            {else}
                                            <a data-id="{$row->MAVE}" href="javascript:;" class="check_ve btn btn-default">
                                                <em class="fa fa-check"></em>
                                            </a>
                                        {/if}
                                    </td>
                                    
                                    <td class="mave hidden-xs">{$row->MAVE}</td>
                                    <td>{$row->TENKHACHHANG}</td>
                                    <td>{$row->GIOKHOIHANH}</td>
                                    <td>{$row->NGAYKHOIHANH}</td>
                                    <td>{$row->MAGHE}</td>
                                    <td class="status_{$row->MAVE}">
                                        {if $row->TrangThai eq 1}
                                            Checked
                                            {else}
                                            No
                                        {/if}
                                    </td>
                                </tr>    
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<style>
    .panel-table .panel-body{
  padding:0;
}

.panel-table .panel-body .table-bordered{
  border-style: none;
  margin:0;
}

.panel-table .panel-body .table-bordered > thead > tr > th:first-of-type {
    text-align:center;
    width: 100px;
}

.panel-table .panel-body .table-bordered > thead > tr > th:last-of-type,
.panel-table .panel-body .table-bordered > tbody > tr > td:last-of-type {
  border-right: 0px;
}

.panel-table .panel-body .table-bordered > thead > tr > th:first-of-type,
.panel-table .panel-body .table-bordered > tbody > tr > td:first-of-type {
  border-left: 0px;
}

.panel-table .panel-body .table-bordered > tbody > tr:first-of-type > td{
  border-bottom: 0px;
}

.panel-table .panel-body .table-bordered > thead > tr:first-of-type > th{
  border-top: 0px;
}

.panel-table .panel-footer .pagination{
  margin:0; 
}

/*
used to vertically center elements, may need modification if you're not using default sizes.
*/
.panel-table .panel-footer .col{
 line-height: 34px;
 height: 34px;
}

.panel-table .panel-heading .col h3{
 line-height: 30px;
 height: 30px;
}

.panel-table .panel-body .table-bordered > tbody > tr > td{
  line-height: 34px;
}
</style>


{/block}