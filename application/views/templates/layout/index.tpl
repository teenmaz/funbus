<!doctype html>
<html class="no-js" lang="en">
  <head>
    <title>{block name=title}Manage FunBus v1 - TPT {/block}</title>
    {block name=head}
        {include file="./head.tpl"}
    {/block}
  </head>
    {block name=body}
        <body class="flat-blue {$custom_class}">
            {block name=main}
                <div class="app-container">
                    <div class="row content-container">
                        <nav class="navbar navbar-default navbar-fixed-top navbar-top">
                            {block name=navbar}{/block}
                        </nav>
                        <div class="side-menu sidebar-inverse">
                            <nav class="navbar navbar-default" role="navigation">
                                {block name=sidebar}{/block}
                            </nav>    
                        </div>  
                        <div class="container-fluid">
                            <div class="side-body padding-top">
                                {block name=content}{/block}
                            </div>
                        </div>
                    </div>
                    {block name=footer}
                        <footer class="app-footer">
                            <div class="wrapper">
                                <span class="pull-right">2.1 <a href="#"><i class="fa fa-long-arrow-up"></i></a></span> © 2015 Copyright.
                            </div>
                        </footer>
                    {/block}
                </div>
            {/block}
            <div id="render-html-modal" class="reveal large" data-reset-on-close="true" data-close-on-click="false" data-reveal></div>
            {block name=foot}
                {include file="./foot.tpl"}
            {/block}
        </body>
    {/block}
</html>