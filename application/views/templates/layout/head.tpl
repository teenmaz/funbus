<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
<!-- CSS Libs -->
<link rel="stylesheet" type="text/css" href="{$asset_url}/css/libs/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{$asset_url}/css/libs/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{$asset_url}/css/libs/animate.min.css">
<link rel="stylesheet" type="text/css" href="{$asset_url}/css/libs/bootstrap-switch.min.css">
<link rel="stylesheet" type="text/css" href="{$asset_url}/css/libs/checkbox3.min.css">
<link rel="stylesheet" type="text/css" href="{$asset_url}/css/libs/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{$asset_url}/css/libs/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="{$asset_url}/css/libs/select2.min.css">
<!-- CSS App -->
<link rel="stylesheet" type="text/css" href="{$asset_url}/css/style.css">
<link rel="stylesheet" type="text/css" href="{$asset_url}/css/flat-blue.css">