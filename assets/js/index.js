$(function() {
    $('.check_ve').on('click', function() {
        var row = $(this);
        var rowid = row.data('id');
        $.get("/admin/change_status/" + rowid, function(data, status) {
            row.removeClass('btn-default');
            row.addClass('btn-success');
            $('.status_' + rowid).text('Checked');
        });
    });

    $('#lotrinh').on('change', function() {
        var lotrinh_id = $(this).val(); // get selected value
        if (lotrinh_id) { // require a URL
            if (lotrinh_id == 0) {
                window.location = window.location.origin + '/admin/bus';
            } else {
                window.location = window.location.origin + '/admin/bus/' + lotrinh_id;
            }
        }
        return false;
    });
});