-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2017 at 07:20 AM
-- Server version: 5.5.54-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbfunbus`
--

-- --------------------------------------------------------

--
-- Table structure for table `chinhanhnhaxe`
--

CREATE TABLE IF NOT EXISTS `chinhanhnhaxe` (
  `TENCN` varchar(255) DEFAULT NULL,
  `MACN` varchar(255) NOT NULL,
  `MAKDNX` varchar(255) NOT NULL,
  `SDTCN` varchar(255) DEFAULT NULL,
  `HINHANHCN` varchar(10) DEFAULT NULL,
  `EMAILCN` char(100) DEFAULT NULL,
  `NAMTL` varchar(5) DEFAULT NULL,
  `DIACHICN` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MACN`),
  KEY `FK_RELATIONSHIP_1` (`MAKDNX`)
) ENGINE=MyISAM;

--
-- Dumping data for table `chinhanhnhaxe`
--

INSERT INTO `chinhanhnhaxe` (`TENCN`, `MACN`, `MAKDNX`, `SDTCN`, `HINHANHCN`, `EMAILCN`, `NAMTL`, `DIACHICN`) VALUES
('FunBus 91B', 'CN1', 'FUNBUS-1992A', '0791221882', 'xe91B.jpg', 'funbus91@gmail.com', '1995', 'số 1,quận Ninh Kiều ,Thành Phố Cần Thơ');

-- --------------------------------------------------------

--
-- Table structure for table `chucvu`
--

CREATE TABLE IF NOT EXISTS `chucvu` (
  `MACHUCVU` int(11) NOT NULL,
  `TENCHUCVU` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MACHUCVU`)
) ENGINE=MyISAM;

--
-- Dumping data for table `chucvu`
--

INSERT INTO `chucvu` (`MACHUCVU`, `TENCHUCVU`) VALUES
(1, 'Quản Lý'),
(2, 'Giám Đốc Chi Nhánh'),
(3, 'Nhân Viên Xe'),
(4, 'Nhân Viên Vé');

-- --------------------------------------------------------

--
-- Table structure for table `ghe`
--

CREATE TABLE IF NOT EXISTS `ghe` (
  `MAGHE` varchar(255) NOT NULL,
  `BIENSOXE` varchar(255) NOT NULL,
  `TRANGTHAI` tinyint(1) DEFAULT NULL,
  `TENGHE` varchar(10) NOT NULL,
  `TENKH` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`MAGHE`),
  KEY `FK_RELATIONSHIP_9` (`BIENSOXE`)
) ENGINE=MyISAM;

--
-- Dumping data for table `ghe`
--

INSERT INTO `ghe` (`MAGHE`, `BIENSOXE`, `TRANGTHAI`, `TENGHE`, `TENKH`) VALUES
('10A', '91A-6556', 2, '10A', 'Le Van A'),
('10B', '91B-6556', 2, '10A', 'Tran Van Nha'),
('10C', '95C-1995', 0, '10A', ''),
('10D', '95D-1992', 0, '10A', ''),
('11A', '91A-6556', 2, '11A', 'Vu Duy Linh'),
('11B', '91B-6556', 0, '11A', ''),
('11C', '95C-1995', 0, '11A', NULL),
('11D', '95D-1992', 0, '11A', NULL),
('12A', '91A-6556', 2, '12A', 'Le Hoang Vinh'),
('12B', '91B-6556', 0, '12A', NULL),
('12C', '95C-1995', 0, '12A', NULL),
('12D', '95D-1992', 0, '12A', NULL),
('13A', '91A-6556', 0, '13A', ''),
('13B', '91B-6556', 0, '13A', NULL),
('13C', '95C-1995', 0, '13A', NULL),
('13D', '95D-1992', 0, '13A', NULL),
('14A', '91A-6556', 0, '14A', ''),
('14B', '91B-6556', 0, '14A', NULL),
('14C', '95C-1995', 0, '14A', NULL),
('14D', '95D-1992', 0, '14A', NULL),
('15A', '91A-6556', 0, '15A', ''),
('15B', '91B-6556', 0, '15A', NULL),
('15C', '95C-1995', 0, '15A', NULL),
('15D', '95D-1992', 0, '15A', NULL),
('16A', '91A-6556', 2, '16B', 'Le Van Ven'),
('16B', '91B-6556', 0, '16B', NULL),
('16C', '95C-1995', 0, '16B', NULL),
('16D', '95D-1992', 0, '16B', NULL),
('17A', '91A-6556', 0, '17B', NULL),
('17B', '91B-6556', 0, '17B', NULL),
('17C', '95C-1995', 0, '17B', NULL),
('17D', '95D-1992', 0, '17B', NULL),
('18A', '91A-6556', 0, '18B', NULL),
('18B', '91B-6556', 0, '18B', NULL),
('18C', '95C-1995', 0, '18B', NULL),
('18D', '95D-1992', 0, '18B', NULL),
('19A', '91A-6556', 0, '19B', ''),
('19B', '91B-6556', 0, '19B', NULL),
('19C', '95C-1995', 0, '19B', NULL),
('19D', '95D-1992', 0, '19B', NULL),
('1A', '91A-6556', 0, '1A', ''),
('1B', '91B-6556', 0, '1A', NULL),
('1C', '95C-1995', 0, '1A', NULL),
('1D', '95D-1992', 0, '1A', NULL),
('20A', '91A-6556', 0, '20B', NULL),
('20B', '91B-6556', 0, '20B', NULL),
('20C', '95C-1995', 0, '20B', NULL),
('20D', '95D-1992', 0, '20B', NULL),
('21A', '91A-6556', 0, '21B', NULL),
('21B', '91B-6556', 0, '21B', NULL),
('21C', '95C-1995', 0, '21B', NULL),
('21D', '95D-1992', 0, '21B', NULL),
('22A', '91A-6556', 0, '22B', NULL),
('22B', '91B-6556', 0, '22B', NULL),
('22C', '95C-1995', 0, '22B', NULL),
('22D', '95D-1992', 0, '22B', NULL),
('23A', '91A-6556', 0, '23B', ''),
('23B', '91B-6556', 0, '23B', NULL),
('23C', '95C-1995', 0, '23B', NULL),
('23D', '95D-1992', 0, '23B', NULL),
('24A', '91A-6556', 0, '24B', ''),
('24B', '91B-6556', 0, '24B', NULL),
('24C', '95C-1995', 0, '24B', NULL),
('24D', '95D-1992', 0, '24B', NULL),
('25A', '91A-6556', 0, '25B', NULL),
('25B', '91B-6556', 0, '25B', NULL),
('25C', '95C-1995', 0, '25B', NULL),
('25D', '95D-1992', 0, '25B', NULL),
('26A', '91A-6556', 0, '26B', NULL),
('26B', '91B-6556', 0, '26B', NULL),
('26C', '95C-1995', 0, '26B', NULL),
('26D', '95D-1992', 0, '26B', NULL),
('27A', '91A-6556', 0, '27B', ''),
('27B', '91B-6556', 0, '27B', NULL),
('27C', '95C-1995', 0, '27B', NULL),
('27D', '95D-1992', 0, '27B', NULL),
('28A', '91A-6556', 0, '28B', ''),
('28B', '91B-6556', 0, '28B', NULL),
('28C', '95C-1995', 0, '28B', NULL),
('28D', '95D-1992', 0, '28B', NULL),
('29A', '91A-6556', 0, '29B', NULL),
('29B', '91B-6556', 0, '29B', NULL),
('29C', '95C-1995', 0, '29B', NULL),
('29D', '95D-1992', 0, '29B', NULL),
('2A', '91A-6556', 0, '2A', NULL),
('2B', '91B-6556', 0, '2A', NULL),
('2C', '95C-1995', 0, '2A', NULL),
('2D', '95D-1992', 0, '2A', NULL),
('30A', '91A-6556', 0, '30B', NULL),
('30B', '91B-6556', 0, '30B', NULL),
('30C', '95C-1995', 0, '30B', NULL),
('30D', '95D-1992', 0, '30B', NULL),
('31D', '95D-1992', 0, '31B', NULL),
('3A', '91A-6556', 0, '3A', NULL),
('3B', '91B-6556', 0, '3A', NULL),
('3C', '95C-1995', 0, '3A', NULL),
('3D', '95D-1992', 0, '3A', NULL),
('4A', '91A-6556', 0, '4A', NULL),
('4B', '91B-6556', 0, '4A', NULL),
('4C', '95C-1995', 0, '4A', NULL),
('4D', '95D-1992', 0, '4A', NULL),
('5A', '91A-6556', 0, '5A', NULL),
('5B', '91B-6556', 0, '5A', NULL),
('5C', '95C-1995', 0, '5A', NULL),
('5D', '95D-1992', 0, '5A', NULL),
('6A', '91A-6556', 0, '6A', NULL),
('6B', '91B-6556', 0, '6A', NULL),
('6C', '95C-1995', 0, '6A', NULL),
('6D', '95D-1992', 0, '6A', NULL),
('7A', '91A-6556', 0, '7A', NULL),
('7B', '91B-6556', 0, '7A', NULL),
('7C', '95C-1995', 0, '7A', NULL),
('7D', '95D-1992', 0, '7A', NULL),
('8A', '91A-6556', 0, '8A', NULL),
('8B', '91B-6556', 0, '8A', NULL),
('8C', '95C-1995', 0, '8A', NULL),
('8D', '95D-1992', 0, '8A', NULL),
('9A', '91A-6556', 2, '9A', 'Le Hoang Vinh'),
('9B', '91B-6556', 0, '9A', NULL),
('9C', '95C-1995', 0, '9A', NULL),
('9D', '95D-1992', 0, '9A', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loaixe`
--

CREATE TABLE IF NOT EXISTS `loaixe` (
  `TENLOAIXE` varchar(255) DEFAULT NULL,
  `MALOAIXE` int(11) NOT NULL,
  PRIMARY KEY (`MALOAIXE`)
) ENGINE=MyISAM;

--
-- Dumping data for table `loaixe`
--

INSERT INTO `loaixe` (`TENLOAIXE`, `MALOAIXE`) VALUES
('Xe Khách', 1),
('Xe Taxi', 2),
('Xe Trung Chuyển', 3),
('Xe Vận Chuyển Hàng', 4);

-- --------------------------------------------------------

--
-- Table structure for table `lotrinh`
--

CREATE TABLE IF NOT EXISTS `lotrinh` (
  `TENLOTRINH` varchar(255) DEFAULT NULL,
  `MALOTRINH` int(11) NOT NULL,
  `MATUYENXE` varchar(255) NOT NULL,
  `THOIGIANLOTRINH` varchar(4) DEFAULT NULL,
  `DIEMDI` varchar(255) DEFAULT NULL,
  `DIEMDEN` varchar(255) DEFAULT NULL,
  `GIA` float DEFAULT NULL,
  `THOIGIANBATDAU` char(10) DEFAULT NULL,
  `TONGKM` varchar(10) NOT NULL,
  PRIMARY KEY (`MALOTRINH`),
  KEY `FK_RELATIONSHIP_8` (`MATUYENXE`)
) ENGINE=MyISAM;

--
-- Dumping data for table `lotrinh`
--

INSERT INTO `lotrinh` (`TENLOTRINH`, `MALOTRINH`, `MATUYENXE`, `THOIGIANLOTRINH`, `DIEMDI`, `DIEMDEN`, `GIA`, `THOIGIANBATDAU`, `TONGKM`) VALUES
('SÀI GÒN-CẦN THƠ', 1, 'T01', '3h', 'Bến Xe Miền Tây', 'BẾN XE 91B', 100000, '8h', '120Km'),
('SÀI GÒN-VĨNH LONG', 2, 'T01', '2h', 'Bến Xe Miền Tây', 'Bến Xe Vĩnh Long', 100000, '8h', '80Km'),
('CẦN THƠ-SÀI GÒN', 3, 'T02', '3h', 'Bến Xe 91B', 'Bến Xe Miền Tây', 100000, '9h', '120Km'),
('CẦN THƠ-VĨNH LONG', 4, 'T02', '2h', 'Bến Xe 91B', 'Bến Xe Vĩnh Long', 100000, '9h', '50Km'),
('CẦN THƠ-CÀ MAU', 5, 'T03', '4h', 'Bến Xe 91B', 'Bến Xe Cà Mau', 150000, '5h', '180Km'),
('CẦN THƠ-BẠC LIÊU', 6, 'T04', '2h', 'Bến Xe 91B', 'Bến Xe Bạc Liêu', 100000, '6h', '100Km'),
('CẦN THƠ-SÀI GÒN', 7, 'T02', '3h', 'Bến Xe 91B', 'Bến Xe Miền Tây', 100000, '16h', '120Km');

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE IF NOT EXISTS `nhanvien` (
  `MANV` varchar(255) NOT NULL,
  `MACHUCVU` int(11) NOT NULL,
  `MACN` varchar(255) NOT NULL,
  `TENNV` varchar(255) DEFAULT NULL,
  `SDTNV` varchar(255) DEFAULT NULL,
  `DIACHINV` varchar(255) DEFAULT NULL,
  `HINHANHNV` text,
  `GIOITINHNV` varchar(255) DEFAULT NULL,
  `NGAYSINH` date DEFAULT NULL,
  `EMAILNV` varchar(255) DEFAULT NULL,
  `MATKHAU` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MANV`),
  KEY `FK_RELATIONSHIP_2` (`MACN`),
  KEY `FK_RELATIONSHIP_3` (`MACHUCVU`)
) ENGINE=MyISAM;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`MANV`, `MACHUCVU`, `MACN`, `TENNV`, `SDTNV`, `DIACHINV`, `HINHANHNV`, `GIOITINHNV`, `NGAYSINH`, `EMAILNV`, `MATKHAU`) VALUES
('1', 3, 'CN1', 'Ngô Hoàng Thái An', '0909991231', 'Ngã 7,Phụng Hiệp,Hậu Giang', NULL, 'Nam', '1994-01-03', 'nanfunbus@gmail.com', 'nanfb'),
('2', 3, 'CN1', 'Trần Quốc Tuấn', '01212119528', 'Ấp 2,Phường 7,Tp Hồ Chí Minh', NULL, 'Nam', '1992-04-05', 'tqtuanfunbus@gmail.com', 'tqtuan'),
('3', 3, 'CN1', 'Trần Văn Nhã', '0167892012', 'Số 1,Lương Tâm,Long Mỹ,Hậu Giang', NULL, 'Nam', '1995-05-19', 'tvnhafunbus@gamil.com', 'nhafunbus'),
('4', 3, 'CN1', 'Huỳnh Tấn Phát', '0932965944', 'số 188,Kế Sách,Sóc Trăng', NULL, 'Nam', '1995-04-15', 'htphatfunbus@gmail.com', 'htphat');

-- --------------------------------------------------------

--
-- Table structure for table `nhaxe`
--

CREATE TABLE IF NOT EXISTS `nhaxe` (
  `MAKDNX` varchar(255) NOT NULL,
  `TENNX` varchar(255) DEFAULT NULL,
  `SDTTRUSO` varchar(255) DEFAULT NULL,
  `DIACHIVPNX` varchar(255) DEFAULT NULL,
  `HINHANHNX` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MAKDNX`)
) ENGINE=MyISAM;

--
-- Dumping data for table `nhaxe`
--

INSERT INTO `nhaxe` (`MAKDNX`, `TENNX`, `SDTTRUSO`, `DIACHIVPNX`, `HINHANHNX`) VALUES
('FUNBUS-1992A', 'FUNBUS', '19008192', '32,Quận 1,Thành Phố Hồ Chí Minh', 'nhaxe.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `phancong`
--

CREATE TABLE IF NOT EXISTS `phancong` (
  `MACONGVIEC` varchar(255) NOT NULL,
  `BIENSOXE` varchar(255) NOT NULL,
  `MATUYENXE` varchar(255) NOT NULL,
  `MANV` varchar(255) NOT NULL,
  `TENCONGVIEC` varchar(255) DEFAULT NULL,
  `NGAYLAMVIEC` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`MACONGVIEC`),
  KEY `FK_RELATIONSHIP_4` (`MANV`),
  KEY `FK_RELATIONSHIP_6` (`MATUYENXE`),
  KEY `FK_RELATIONSHIP_7` (`BIENSOXE`)
) ENGINE=MyISAM;

--
-- Dumping data for table `phancong`
--

INSERT INTO `phancong` (`MACONGVIEC`, `BIENSOXE`, `MATUYENXE`, `MANV`, `TENCONGVIEC`, `NGAYLAMVIEC`) VALUES
('CV1', '91A-6556', 'T01', '1', 'Tài Xế', '12-04-2017'),
('CV2', '91B-6556', 'T02', '2', 'Tài Xế', '13-04-2017'),
('CV3', '95C-1995', 'T03', '3', 'Tài Xế', '14-04-2017'),
('CV4', '95D-1992', 'T04', '4', 'Tài Xế', '15-04-2017');

-- --------------------------------------------------------

--
-- Table structure for table `phuongthucthanhtoan`
--

CREATE TABLE IF NOT EXISTS `phuongthucthanhtoan` (
  `TENPHUONGTHUC` varchar(255) DEFAULT NULL,
  `MAPHUONGTHUC` int(11) NOT NULL,
  PRIMARY KEY (`MAPHUONGTHUC`)
) ENGINE=MyISAM;

--
-- Dumping data for table `phuongthucthanhtoan`
--

INSERT INTO `phuongthucthanhtoan` (`TENPHUONGTHUC`, `MAPHUONGTHUC`) VALUES
('Thanh Toán Tại Quầy Vé', 1),
('Thanh Toán Qua Thẻ', 2);

-- --------------------------------------------------------

--
-- Table structure for table `taikhoandv`
--

CREATE TABLE IF NOT EXISTS `taikhoandv` (
  `SDT` varchar(255) NOT NULL,
  `MAPHUONGTHUC` int(11) NOT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `MATKHAUKH` varchar(255) DEFAULT NULL,
  `TENDN` varchar(50) NOT NULL,
  PRIMARY KEY (`SDT`),
  KEY `FK_RELATIONSHIP_12` (`MAPHUONGTHUC`)
) ENGINE=MyISAM;

--
-- Dumping data for table `taikhoandv`
--

INSERT INTO `taikhoandv` (`SDT`, `MAPHUONGTHUC`, `EMAIL`, `MATKHAUKH`, `TENDN`) VALUES
('01212119517', 1, 'giang@gmail.com', '123123', 'Phan Văn Giảng'),
('01212119518', 0, 'ven@gamil.com', '123123', 'Lê Van Ven'),
('01231111111', 1, 'abc111@gmail.com', '123123', 'kokokko'),
('01232222222', 1, 'abc222@gmail.com', '123123', 'fafafafa'),
('01697382012', 1, 'nha@gmail.com', '123123', 'Tran Van Nha'),
('0907856907', 0, 'vdlinh@ctu.edu.vn', '123123', 'Vu Duy Linh '),
('0932954942', 1, 'bich@gmail.com', '123123', 'Tran Ngoc Bich'),
('0932955933', 0, 'bich@gmail.com', '123123', 'Bich'),
('0932955944', 0, 'bichgiang@gmail.com', '123123', 'bich tran'),
('0933944955', 0, 'Huynh@gmail.com', '123123', 'Le Huynh');

-- --------------------------------------------------------

--
-- Table structure for table `tuyenxe`
--

CREATE TABLE IF NOT EXISTS `tuyenxe` (
  `MATUYENXE` varchar(255) NOT NULL,
  `TENTUYENXE` varchar(255) DEFAULT NULL,
  `SOTRAMDUNG` int(11) DEFAULT NULL,
  PRIMARY KEY (`MATUYENXE`)
) ENGINE=MyISAM;

--
-- Dumping data for table `tuyenxe`
--

INSERT INTO `tuyenxe` (`MATUYENXE`, `TENTUYENXE`, `SOTRAMDUNG`) VALUES
('T01', 'SÀI GÒN-CẦN THƠ', 2),
('T02', 'CẦN THƠ-SÀI GÒN', 2),
('T03', 'CẦN THƠ-CÀ MAU', 3),
('T04', 'CẦN THƠ-BẠC LIÊU', 2),
('T05', 'CÀ MAU-CẦN THƠ', 2),
('T06', 'BẠC LIÊU-CẦN THƠ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ve`
--

CREATE TABLE IF NOT EXISTS `ve` (
  `MAVE` int(11) NOT NULL AUTO_INCREMENT,
  `MALOTRINH` int(11) NOT NULL,
  `SDT` varchar(255) NOT NULL,
  `TONGTIEN` float DEFAULT NULL,
  `TENKHACHHANG` varchar(255) DEFAULT NULL,
  `GIOKHOIHANH` varchar(10) DEFAULT NULL,
  `NGAYKHOIHANH` varchar(20) NOT NULL,
  `MAGHE` varchar(10) DEFAULT NULL,
  `TENGHE` varchar(10) DEFAULT NULL,
  `TrangThai` tinyint(1) NOT NULL,
  PRIMARY KEY (`MAVE`),
  KEY `FK_RELATIONSHIP_10` (`MALOTRINH`),
  KEY `FK_RELATIONSHIP_11` (`SDT`)
) ENGINE=MyISAM  AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ve`
--

INSERT INTO `ve` (`MAVE`, `MALOTRINH`, `SDT`, `TONGTIEN`, `TENKHACHHANG`, `GIOKHOIHANH`, `NGAYKHOIHANH`, `MAGHE`, `TENGHE`, `TrangThai`) VALUES
(1, 1, '01231111111', 100000, 'Le Hoang Vinh', '8h', '12-04-2017', '9A', '9A', 0),
(3, 1, '0932954942', 100000, 'Le Van A', '8h', '12-04-2017', '10A', '10A', 0),
(4, 3, '01697382012', 100000, 'Tran Van Nha', '9h', '13-04-2017', '10B', '10A', 0),
(5, 1, '01212119518', 100000, 'Le Van Ven', '8h', '12-04-2017', '16A', '16B', 0),
(6, 1, '0907856907', 100000, 'Vu Duy Linh', '8h', '12-04-2017', '11A', '11A', 0),
(7, 1, '0907856907', 100000, 'Le Hoang Vinh', '8h', '12-04-2017', '12A', '12A', 0);

-- --------------------------------------------------------

--
-- Table structure for table `xe`
--

CREATE TABLE IF NOT EXISTS `xe` (
  `BIENSOXE` varchar(255) NOT NULL,
  `MALOAIXE` int(11) NOT NULL,
  `SOGHE` int(11) DEFAULT NULL,
  `GHICHU` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`BIENSOXE`),
  KEY `FK_RELATIONSHIP_5` (`MALOAIXE`)
) ENGINE=MyISAM;

--
-- Dumping data for table `xe`
--

INSERT INTO `xe` (`BIENSOXE`, `MALOAIXE`, `SOGHE`, `GHICHU`) VALUES
('91A-6556', 1, 30, 'Wifi,Máy điều hòa,nước,khăn lạnh.'),
('91B-6556', 1, 30, 'Wifi,Máy điều hòa,nước,khăn lạnh.'),
('95C-1995', 1, 30, 'Có máy lạnh,wifi,nước suối'),
('95D-1992', 1, 35, 'Có Máy lạnh,wifi,nước suối');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chinhanhnhaxe`
--
ALTER TABLE `chinhanhnhaxe`
  ADD CONSTRAINT `FK_RELATIONSHIP_1` FOREIGN KEY (`MAKDNX`) REFERENCES `nhaxe` (`MAKDNX`);

--
-- Constraints for table `ghe`
--
ALTER TABLE `ghe`
  ADD CONSTRAINT `FK_RELATIONSHIP_9` FOREIGN KEY (`BIENSOXE`) REFERENCES `xe` (`BIENSOXE`);

--
-- Constraints for table `lotrinh`
--
ALTER TABLE `lotrinh`
  ADD CONSTRAINT `FK_RELATIONSHIP_8` FOREIGN KEY (`MATUYENXE`) REFERENCES `tuyenxe` (`MATUYENXE`);

--
-- Constraints for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD CONSTRAINT `FK_RELATIONSHIP_2` FOREIGN KEY (`MACN`) REFERENCES `chinhanhnhaxe` (`MACN`),
  ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`MACHUCVU`) REFERENCES `chucvu` (`MACHUCVU`);

--
-- Constraints for table `phancong`
--
ALTER TABLE `phancong`
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`MANV`) REFERENCES `nhanvien` (`MANV`),
  ADD CONSTRAINT `FK_RELATIONSHIP_6` FOREIGN KEY (`MATUYENXE`) REFERENCES `tuyenxe` (`MATUYENXE`),
  ADD CONSTRAINT `FK_RELATIONSHIP_7` FOREIGN KEY (`BIENSOXE`) REFERENCES `xe` (`BIENSOXE`);

--
-- Constraints for table `ve`
--
ALTER TABLE `ve`
  ADD CONSTRAINT `FK_RELATIONSHIP_10` FOREIGN KEY (`MALOTRINH`) REFERENCES `lotrinh` (`MALOTRINH`),
  ADD CONSTRAINT `FK_RELATIONSHIP_11` FOREIGN KEY (`SDT`) REFERENCES `taikhoandv` (`SDT`);

--
-- Constraints for table `xe`
--
ALTER TABLE `xe`
  ADD CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`MALOAIXE`) REFERENCES `loaixe` (`MALOAIXE`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
